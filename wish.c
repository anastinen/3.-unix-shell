/* Antti Vilkman 0521281
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * 12/2018
 * Harjoitustyö Unix Shell
 * wish.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define error_message "An error has occurred\n"

char *path = NULL;

char **parseArgs(char *);
void chooseCommand(char ***, int);
void wish_cd(char **);
void wish_path(char **);
void executeExecutable(char **);


int main(int argc, char *argv[])
{

	char *line = NULL; 
	char *token = NULL;
	char ***args = NULL;
	char **lines = NULL;
	size_t linecap = 0;
	int parals = 0;
	int i = 0;

	if ((path = (char *) malloc(strlen("/bin")+1))) {
		strcpy(path, "/bin");
	}
	else {
		write(STDERR_FILENO, "Failed to allocate memory for path\n", strlen("Failed to allocate memory for path\n"));	
	}
	// Interactive mode
	if (argc < 2) {
		while(1) {
			printf("wish> ");

			// Get the command line 
			getline(&line, &linecap, stdin);

			// Create array of parallel commands to parse
			if(!(lines = calloc(64, sizeof(char*)))) {
				write(STDERR_FILENO, "Failed to allocate memory for lines\n", strlen("Failed to allocate memory for lines\n"));
				continue;
			}
			token = strtok(line, "&");
			i = 0;
			while (token != NULL) {
				lines[i] = token;
				i++;
				parals++;
				token = strtok(NULL, "&");
			}
			lines[i++] = NULL;

			// Allocate memory for parsed commands
			if (!(args = calloc(64, sizeof(char**)))) {
				write(STDERR_FILENO, "Failed to allocate memory for commands\n", strlen("Failed to allocate memory for commands\n"));
				continue;
			}
			// Parse the commands
			for (i = 0; lines[i] != NULL; i++) {
				args[i] = parseArgs(lines[i]);
			}
			// Check what command was given and do according things
			chooseCommand(args, parals);

			// Free the allocated memory
			free(lines);
			lines = NULL;
			free(args);	
			args = NULL;	
		}
	}
	// Batch mode
	else if (argc <= 3) {
		FILE *f;
		if((f = fopen(argv[1], "r"))) {
			while (!feof(f)) {
				getline(&line, &linecap, f);
				// Create array of parallel commands to parse
				if(!(lines = calloc(64, sizeof(char*)))) {
					write(STDERR_FILENO, "Failed to allocate memory for lines\n", strlen("Failed to allocate memory for lines\n"));
					continue;
				}
				token = strtok(line, "&");
				i = 0;
				while (token != NULL) {
					lines[i] = token;
					i++;
					parals++;
					token = strtok(NULL, "&");
				}
				lines[i++] = NULL;

				// Allocate memory for parsed commands
				if (!(args = calloc(64, sizeof(char**)))) {
					write(STDERR_FILENO, "Failed to allocate memory for commands\n", strlen("Failed to allocate memory for commands\n"));
					continue;
				}
				// Parse the commands
				for (i = 0; lines[i] != NULL; i++) {
					if(!(args[i] = parseArgs(lines[i]))) {
						write(STDERR_FILENO, "NULL from parsing\n", strlen("NULL from parsing\n"));
						break;
					}
				}
				// Check what command was given and do according things
				chooseCommand(args, parals);

				// Free the allocated memory
				free(lines);
				lines = NULL;
				free(args);	
				args = NULL;	
			}	
			exit(0);
		}
		// If file failed to open
		else {
			exit(1);
		}
	}	
	// If there is more than one file
	else {
		exit(1);
	}
}


char **parseArgs(char *line) 
{
	char *token = NULL;
	char *outFile = NULL;
	char *mult_redir_check = calloc(sizeof(char*), strlen(line)+1);
	int redirection = 0;
	int redirectedAlready = 0;

	strcpy(mult_redir_check, line);
	// Check if there's redirection
	token = strtok(line, ">");
	// Check if there's a destination given after redirection
	if ((outFile = strtok(NULL, ">\n\t "))) {
		redirection = 1;
		// Check for multiple redirection symbols
		
		for (int i = 0; mult_redir_check[i] != '\0'; i++) {
			if (mult_redir_check[i] == '>' && redirectedAlready) {
				write(STDERR_FILENO, "Too many redirection operators\n", strlen("Too many redirection operators\n"));
				return NULL;
			}
			else if (mult_redir_check[i] == '>') {
				redirectedAlready = 1;
		} 
	}
		// Make sure there's only a single output
		if (strtok(NULL, ">\n ")) {
			write(STDERR_FILENO, "Too many args for redirection\n", strlen("Too many args for redirection\n"));
			return NULL;
		}
	}
	free(mult_redir_check);
	// Parse args separated by various whitespace
	token = strtok(token, "\n\t ");
	if (!token) {
		return NULL;
	}

	// Create a dynamic array to store the given arguments in
	int counter = 0;
	int argsSize = 64;
	char **args = malloc(argsSize * sizeof(char*));
	// If memory allocation fails, exit
	if (!args) {
    	write(STDERR_FILENO, "Failure while allocating memory for args\n", strlen("Failure while allocating memory for args\n"));
    	return NULL;
  	}
  	// Add all the arguments to an array
	while (token != NULL) {
    	args[counter] = token;
    	counter++;
    	// If array runs out of space, allocate more
    	if (counter >= argsSize) {
      		argsSize += argsSize;
      		args = realloc(args, argsSize * sizeof(char*));
      		if (!args) {
      			write(STDERR_FILENO, "Failure while reallocating memory for args\n", strlen("Failure while reallocating memory for args\n"));
      			return NULL;
      		}
    	}
    	token = strtok(NULL, "\n\t ");
    }
    // If redirection is on, add an indicator and filename to the end of args for the commands
    if (redirection) {
    	args[counter] = ">";
    	counter++;
    	args[counter] = outFile;
    	counter++;
    }
    args[counter] = NULL;
    return args;
}


void chooseCommand(char ***args, int parals) 
{
	pid_t pids[parals];
	int n = parals;
	int status;
	pid_t pid;
	for (int a = 0; args[a] != NULL; a++) {
		// If we find exit, we just exit, not forking that
		if (!strcmp("exit", args[a][0])) { 	
			exit(0);
		}
		// If given command is cd
		if (!strcmp("cd", args[a][0])) { 	
			wish_cd(args[a]);
		}
		// If given command is path
		else if (!strcmp("path", args[a][0])) {	
			wish_path(args[a]);
		}
		// If given command is not a built-in
		else {
			// Fork parallel commands, built-ins didn't get this because it messed things up
			//printf("Here\n");
			if ((pids[a] = fork()) < 0) {
				write(STDERR_FILENO, "Failed to fork parallel command\n", strlen("Failed to fork parallel command\n"));
				abort();
			}
			else if (pids[a] == 0) {
				executeExecutable(args[a]);
				exit(0);
			}
		}
	}
	while (n>0) {
		pid = wait(&status);
		for (int i = 0; i < parals; i++) {
			if (pid == pids[i]) {
				pids[i] = 0;
			}
		}
		--n;
	}
}


void wish_cd(char **args)
{
	// If there is more than one argument
	if (args[2]) { 	
		write(STDERR_FILENO, "cd only takes one argument\n", strlen("cd only takes one argument\n"));
		return;
	}
	// If there is a given dir and input is valid, change to given directory
	else if (args[1]) { 	
		chdir(args[1]);
	}
	// If no directory has been given to cd, print error message
	else { 		
		write(STDERR_FILENO, "No directory given, please try again\n", strlen("No directory given, please try again\n"));
		return;
	}
} 


void wish_path(char **args) 
{
	char wholepath[100] = "";
	free(path);
	if (args[1] == NULL) {
		// If given path was empty
		if((path = (char *) malloc(strlen("\0")+1))) {
			strcpy(path, "\0");
		}
		else {
			write(STDERR_FILENO, "Failed to allocate memory for path\n", strlen("Failed to allocate memory for path\n"));
		}
	}
	else {
		// Add all given paths to the entire path
		for (int i = 1; args[i] != NULL; i++) {
			strcat(wholepath, args[i]);
			strcat(wholepath, " ");
		}
		if ((path = (char *) malloc(strlen(wholepath)+1))) {
			strcpy(path, wholepath);
		}
		else {
			write(STDERR_FILENO, "Failed to allocate memory for path\n", strlen("Failed to allocate memory for path\n"));
		}
	}
}


void executeExecutable (char **args) {
	pid_t pid, wpid;
	int status;
	char *token;
	char exact_path[100]; 
	char *output;
	int std_out;
	int redirection = 0;

	// Check if executable exists in any of the given paths
	token = strtok(path, "\n ");
	while (token != NULL) {
    	sprintf(exact_path, "%s/%s", token, args[0]);
    	// If the executable was found
    	if (!access(exact_path, X_OK)) {
    		// Launch the given process
    		pid = fork();
    		if (pid == 0) {
    			// Check for redirection
    			for (int i = 0; args[i] != NULL; i++) {
    				if (!strcmp(args[i], ">")) {
    					redirection = 1;
    					// Save output destination, set the redirection arguments to null so they dont go into the file
    					output = malloc(strlen(args[i+1]) + 1);
    					strcpy(output, args[i+1]);
    					args[i+1] = NULL;
    					args[i] = NULL;
    					break;
    				}
    			}
    			// If output redirection is on
    			if (redirection) {
    				std_out = dup(1);
    				// Redirect output to the destination that was given
    				int fd = creat(output, 0644);
    				dup2(fd, 1);
    				close(fd);
    			}	
    			// Try to execute command
    			if (execv(exact_path, args) == -1) {
    				write(STDERR_FILENO, "Failed to execute executable\n", strlen("Failed to execute executable\n"));
    				return;
    			}
    			// Reconnect output to stdout
    			if (redirection) {
	    			dup2(std_out, 1);
	    			close(std_out);
	    			free(output);
	    			redirection = 0;
	    		}

    		}
    		else if (pid < 0) {
    			write(STDERR_FILENO, "Failed to fork\n", strlen("Failed to fork\n"));
    			return;
    		}
    		else {
    			// Wait for child process to exit
    			do {
    				wpid = waitpid(pid, &status, WUNTRACED);
    			} while (!WIFEXITED(status) && !WIFSIGNALED(status));
    		}
    		return; 
    	}
    	// If the executable was not found, try the next token in path
    	token = strtok(NULL, "\n ");
	}
	// If the executable is not in path, give error message
	write(STDERR_FILENO, "Executable not found in path\n", strlen("Executable not found in path\n"));
}
